# Session II: NTUC Club

NTUC Club is one of our top 10 partners, contributing significantly to our revenue (over $14m GMV in 2018). It is owned directly by NTUC Union, and doesn't come under the NTUC Enterprise umbrella like Link and FairPrice.

When we refer to NTUC Club, we don't just consider the entities that it owns, such as D'Resort, Wild Wild Wet, and Orchid Bowl. All the other external merchants that are located at the Downtown East shopping mall and issue LinkPoints also belong to this account. A detailed merchant listing can be found in this folder, as well as at lac.ntucclub_merchantlisting

We have expanded our partnership with them beyond LinkPoints and campaigns. In 2019, we produced a dashboard for them, and this contract has a duration of 2 years. We want to build on this success and deliver on Data Insights Phase II when the time is right.