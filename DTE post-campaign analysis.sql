-- WWW: 587
select * from link.link_cmp_promotion where upper(promotion_name) like '%WWW%';
select * from link.link_cls_branch where upper(branch_name_english_1) like '%WILD%';

-- 145,019
select count (distinct(a.csn)) from link.link_fact_cmp_campaign_email_stats a
JOIN link.link_cmp_promotion b ON a.promotion_id = b.promotion_id_pk
where b.promotion_id_pk = 587;

-- 16,897
select count (distinct(a.csn)) from link.link_fact_cmp_campaign_email_stats a
JOIN link.link_cmp_promotion b ON a.promotion_id = b.promotion_id_pk
where b.promotion_id_pk = 587 and (open_ind=1 or click=1);

-- converted
SELECT distinct(a.csn), b.promotion_name,	b.campaign_id_fk,campaign_name,a.open_ind,a.click,a.bounce,y.corporate_name_english_1, est_gross_transaction_amt
FROM 	link.link_fact_cmp_campaign_email_stats a 
JOIN link.link_cmp_promotion b ON a.promotion_id = b.promotion_id_pk
JOIN link.link_cmp_campaign c ON b.campaign_id_fk = c.campaign_id_pk
right JOIN link.view_facttrans_plus x on x.csn=a.csn
join link.link_cls_corporation y on x.corporation_id=y.corporate_id
where
b.promotion_id_pk = 587
and
x.transaction_date  between '2018-02-07' and  '2018-03-07' 
and x.corporation_id=103300000000 and x.branch_no=1033018880
and 
x.POOL_ID='P01' and x.CLUB_CODE ='LNK' and x.CANCEL_IND = 'N' and x.pointID = 'I' and x.est_gross_transaction_amt>0
and (open_ind=1 or click=1)
order by y.corporate_name_english_1, open_ind, click;

-- World Cup 
select * from link.link_cmp_promotion where upper(promotion_name) like '%WORLD%';

-- 14 Jun to 15 Jul

select 'campaign' as period, count(distinct csn), sum(est_gross_transaction_amt), sum(transcount)
from link.view_facttrans_plus a
join LAC.manf_downtowneastmarketsq b on a.corporation_id=b.corporation_id and branch_no=branch_id
where transaction_date>='2018-06-14' and transaction_date<='2018-07-15'
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointID='I' and est_gross_transaction_amt>0
union
select 'pre' as period, count(distinct csn), sum(est_gross_transaction_amt), sum(transcount)
from link.view_facttrans_plus a
join LAC.manf_downtowneastmarketsq b on a.corporation_id=b.corporation_id and branch_no=branch_id
where transaction_date>='2018-05-12' and transaction_date<='2018-06-13'
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointID='I' and est_gross_transaction_amt>0
union
select '2017' as period, count(distinct csn), sum(est_gross_transaction_amt), sum(transcount)
from link.view_facttrans_plus a
join LAC.manf_downtowneastmarketsq b on a.corporation_id=b.corporation_id and branch_no=branch_id
where transaction_date>='2017-06-14' and transaction_date<='2017-07-15'
and pool_id='P01' and club_code='LNK' and cancel_ind='N' and pointID='I' and est_gross_transaction_amt>0;
